app.controller('navbarCtrl', function($scope, $window, $location, authenticationService) {

  var vm = this;

  vm.isLoggedIn = authenticationService.isLoggedIn();

  vm.currentUser = authenticationService.currentUser();

  vm.logout = function() {
    authenticationService.logout();
    $location.path('/login');
  };

});
