app.controller('profileCtrl', function ($location, profileService) {
  var vm = this;

  vm.user = {};

  profileService.getProfile()
    .success(function(data) {
      vm.user = data;
    })
    .error(function (e) {
      console.log(e);
    });

});
