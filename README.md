# vizualizacia-genetickych-analyz

1) Nainstalovat Node a npm - node package manager
https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
test: v konzole zadat commandy node -v a npm -v a malo by vratit verzie, vtedy su v poriadku nainstalovane.

2) Nainstalovat a nakonfigurovat MongoDB:
navod pre linux https://docs.mongodb.org/manual/administration/install-on-linux/
navod pre windows https://docs.mongodb.org/manual/tutorial/install-mongodb-on-windows/

3) nainstalovat Bower package manager pre frontend
npm install -g bower

4) Nainstalovat Grunt command line interface
npm install -g grunt-cli

5) V korenovom adresary projektu zavolat prikazy
npm install 
- nainstaluje backend dependencies zapisane v package.json
bower install
- nainstaluje frontend dependencies zapisane v bower.json

6) pre spustenie developement servera zavolat prikaz: grunt serve
 - spusti developement server a otvori okno prehliadaca a spusti v nom aplikaciu na localhost:3000, 
 dokaze sledovat zmeny v suboroch projektu ako javascript, html, css subory a pri ich zmene automaticky restartuje server a aplikuje zmeny
 
 alebo pre spustenie serveru zadat prikaz node server/server
 - v zlozke server v adresary projektu spusti hlavny subor serveru s nazvom server.js
  v konzole bude vidno hlasku: Server listening at port 3000. V okne prehliadaca otvorit adresu localhost:3000 pre pristup k aplikacii`
