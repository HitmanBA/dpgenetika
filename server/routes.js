/**
 * Created by Hitman on 7. 3. 2016.
 */

var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

var projects = require('./controllers/projects-controller');
var ctrlAuth = require('./controllers/authentication');
var ctrlProfile = require('./controllers/profile');

router.get('/projects', projects.list);
router.post('/projects', projects.create);
router.delete('/projects/:projectID', projects.delete);

router.get('/profile', auth, ctrlProfile.profileRead);

router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);
router.get('/users', ctrlAuth.getUsers);
router.delete('/users/:userID', ctrlAuth.delUsers);

module.exports = router;
