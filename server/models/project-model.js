/**
 * Created by Hitman on 7. 3. 2016.
 */

var mongoose = require('mongoose');

module.exports = mongoose.model('Project', {
  date: Date,
  name: String,
  username: String
});
