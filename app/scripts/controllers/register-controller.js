app.controller('registerCtrl', function ($location, authenticationService) {
  var vm = this;

  vm.viewby = 5;
  vm.maxSize = 5;

  vm.sortType = 'email';
  vm.sortReverse = true;
  vm.searchBar = '';

  vm.credentials = {
    name : "",
    email : "",
    password : "",
    role: 'user'
  };

  vm.users = [];

  vm.passwordChk = '';
  vm.messg = '';
  vm.showError = false;

  vm.getUsers = function () {
    return authenticationService.queryUsers().
      then(function(data){
        vm.users = data;
        vm.totalItems = vm.users.length;
        return vm.users;
      }, function(error){
        console.error('Error occured', error);
      });
  };

  vm.equalPass = function() {
    if(vm.passwordChk === vm.credentials.password) {
      vm.messg = '';
      return vm.showError = false;
    }
    else {
      vm.messg = 'Heslá sa nezhodujú';
      return vm.showError = true;
    }
  };

  vm.checkNames = function(attribute) {
    vm.showError = false;
    angular.forEach(vm.users, function(value, key){
      if(attribute === 'name') {
        if(value.name === vm.credentials.name) {
          vm.messg = 'Užívateľské meno je už zaregistrované, zvoľte prosím iné.';
          return vm.showError = true;
        }
      }
      if(attribute === 'email') {
        if(value.email === vm.credentials.email) {
          vm.messg = 'E-mail je už zaregistrovaný, zvoľte prosím iný.';
          return vm.showError = true;
        }
      }
    })
  };

  vm.onSubmit = function () {

    authenticationService
      .register(vm.credentials)
      .error(function(err){
        alert(err);
      })
      .then(function(){
      });

    vm.getUsers();
  };

  vm.potvrdit = function(num, id) {
    if(num === 1) {
      $('#' + id + 1).addClass('hidden');
      $('#' + id + 2).removeClass('hidden');
    }
    if(num === 2) {
      $('#' + id + 2).addClass('hidden');
      $('#' + id + 1).removeClass('hidden');
    }
  };

  vm.vymazat = function (idUsera) {
    vm.potvrdit(2, idUsera);
    authenticationService.del({userID: idUsera})
      .then(function(data){
      }, function(error) {
        console.error('Error occured', error);
      });
    vm.getUsers();
  };


  vm.getUsers();
});
