app.directive('navigation', navigation);

function navigation () {
  return {
    restrict: 'EA',
    templateUrl: '../../views/navigation.html',
    controller: 'navbarCtrl as navvm'
  };
}
