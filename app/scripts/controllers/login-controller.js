app.controller('loginCtrl', function ($location, authenticationService) {
  var vm = this;

  vm.credentials = {
    email : "",
    password : ""
  };

  vm.messg = '';
  vm.fail = false;

  vm.onSubmit = function () {
    authenticationService
      .login(vm.credentials)
      .error(function(err){
        console.log(err.message);
        vm.messg = err.message;
        vm.fail = true;
      })
      .then(function(){
        $location.path('/');
      });
  };

});
