/**
 * Created by Hitman on 7. 3. 2016.
 */

app.factory('dashboardService', function($http, $resource){
  var projectObject = {
    query: query,
    create: create,
    del: del
  };

  var dashboardResource = $resource('/api/projects/:projectID', {projectID: '@id'});

  return projectObject;

  function query() {
    return dashboardResource.query().$promise;
  }

  function create(data){
    return dashboardResource.save(data).$promise;
  }

  function del(id) {
    return dashboardResource.delete(id).$promise;
  }

});
