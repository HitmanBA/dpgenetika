'use strict';

/**
 * @ngdoc function
 * @name vizualizaciaGenetickychAnalyzApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the vizualizaciaGenetickychAnalyzApp
 */
angular.module('vizualizaciaGenetickychAnalyzApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
