/**
 * Created by Hitman on 7. 3. 2016.
 */

app.factory('authenticationService', function($http, $window, $resource){

  var usersResource = $resource('/api/users/:userID', {userID: '@id'});

  var saveToken = function (token) {
    $window.localStorage['mean-token'] = token;
  };

  var getToken = function () {
    return $window.localStorage['mean-token'];
  };

  var isLoggedIn = function() {
    var token = getToken();
    var payload;

    if(token){
      payload = token.split('.')[1];
      payload = $window.atob(payload);
      payload = JSON.parse(payload);

      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  };

  var currentUser = function() {
    if(isLoggedIn()){
      var token = getToken();
      var payload = token.split('.')[1];
      payload = $window.atob(payload);
      payload = JSON.parse(payload);
      return {
        email : payload.email,
        name : payload.name,
        role : payload.role
      };
    }
  };

  register = function(user) {
    return $http.post('/api/register', user).success(function(){
    });
  };

  login = function(user) {
    return $http.post('/api/login', user).success(function(data) {
      saveToken(data.token);
    });
  };

  logout = function() {
    $window.localStorage.removeItem('mean-token');
  };

  function queryUsers() {
    return usersResource.query().$promise;
  }

  function del(id) {
    return usersResource.delete(id).$promise;
  }

  return {
    currentUser : currentUser,
    saveToken : saveToken,
    getToken : getToken,
    isLoggedIn : isLoggedIn,
    register : register,
    login : login,
    logout : logout,
    queryUsers: queryUsers,
    del: del
  };

});
