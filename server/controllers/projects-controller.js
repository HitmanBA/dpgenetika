/**
 * Created by Hitman on 7. 3. 2016.
 */

var Project = require('../models/project-model');
//var bodyParser = require('body-parser');

//var urlEncoded = bodyParser.urlencoded({extended: false});

module.exports.list = function(req, res) {
  Project.find({}, function(err, results){
    if(err)
      res.send(err);

    res.json(results);
  });
};

module.exports.create = function(req, res) {
  var project = new Project(req.body);
  project.save(function(err, results){
    if(err)
      res.send(err);

    res.status(201).json(results);
  });
};

module.exports.delete = function(req, res) {
  Project.remove({
    _id: req.params.projectID
  }, function (err, project) {
    if(err)
      res.send(err);

    res.sendStatus(200);
  });
};
