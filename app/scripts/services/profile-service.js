/**
 * Created by Hitman on 7. 3. 2016.
 */

app.factory('profileService', function($http, authenticationService){

  var getProfile = function () {
    return $http.get('/api/profile', {
      headers: {
        Authorization: 'Bearer '+ authenticationService.getToken()
      }
    });
  };

  return {
    getProfile : getProfile
  };

});
