'use strict';

app.controller('DashboardController', function ($scope, dashboardService, authenticationService) {
  $scope.projects = [];
  $scope.existutje = false;
  $scope.viewby = 5;
  $scope.maxSize = 5;

  $scope.sortType = 'date';
  $scope.sortReverse = true;
  $scope.searchBar = '';

  var currentUser = authenticationService.currentUser();

  getProjects();

  function getProjects() {
    return dashboardService.query().
      then(function(data){
        $scope.projects = data;
        $scope.totalItems = $scope.projects.length;
        return $scope.projects;
      }, function(error){
        console.error('Error occured', error);
      });
  }

  function newProject(project) {
    return dashboardService.create(project)
      .then(function(data) {
        $scope.projects.push(data);
        $scope.totalItems = $scope.projects.length;
        $scope.name = '';
      }, function(error){
        console.error('Error occurred', error);
      });
  }

  $scope.skontroluj = function() {
    $scope.existutje = false;
    angular.forEach($scope.projects, function(value, key){
      if(value.name === $scope.name) {
        return $scope.existutje = true;
      }
    })
  };

  $scope.saveProject = function(){
    var project = {date: Date.now(), name: $scope.name, username: currentUser.name};
    newProject(project);
  };

  $scope.vymazat = function (idProjektu) {
    $scope.potvrdit(2, idProjektu);
    dashboardService.del({projectID: idProjektu})
      .then(function(data){
      }, function(error) {
        console.error('Error occured', error);
      });
    getProjects();
  };

  $scope.potvrdit = function(num, id) {
    if(num === 1) {
      $('#' + id + 1).addClass('hidden');
      $('#' + id + 2).removeClass('hidden');
    }
    if(num === 2) {
      $('#' + id + 2).addClass('hidden');
      $('#' + id + 1).removeClass('hidden');
    }
  };

});
