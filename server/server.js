/**
 * Created by Hitman on 6. 3. 2016.
 */

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var database = require('./config/database');
var methodOverride = require('method-override');
var passport = require('passport');

mongoose.connect(database.url);
require('./models/users-model');
require('./config/passport');

var routesApi = require('./routes');

var app = express();

app.use(express.static(__dirname + '/../app'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cookieParser());

app.use(passport.initialize());

app.use('/api', routesApi);

app.use(function(req, res) {
  res.sendfile('./app/views/index.html');
});

app.listen(3000, function() {
  console.log('Server listening at port 3000...');
});
