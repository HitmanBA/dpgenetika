'use strict';

/**
 * @ngdoc overview
 * @name vizualizaciaGenetickychAnalyzApp
 * @description
 * # vizualizaciaGenetickychAnalyzApp
 *
 * Main module of the application.
 */
var app = angular.module('vizualizaciaGenetickychAnalyzApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'ui.bootstrap',
  'angularUtils.directives.dirPagination'
]);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '../views/dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'main',
        access: {restricted: true}
      })
      .when('/register', {
        templateUrl: '../views/register.html',
        controller: 'registerCtrl',
        controllerAs: 'vm',
        access: {restricted: true}
      })
      .when('/login', {
        templateUrl: '../views/login.html',
        controller: 'loginCtrl',
        controllerAs: 'vm',
        access: {restricted: false}
      })
      .when('/profile', {
        templateUrl: '../views/profile.html',
        controller: 'profileCtrl',
        controllerAs: 'vm',
        access: {restricted: true}
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about',
        access: {restricted: true}
      })
      .otherwise({
        redirectTo: '/',
        access: {restricted: true}
      });

  $locationProvider.html5Mode(true);
});

app.run( function($rootScope, $location, authenticationService){
  $rootScope.$on('$routeChangeStart', function (event, next, $route) {
    if ($location.path() === '/' && !authenticationService.isLoggedIn()) {
      $location.path('/');
    }
    if (next.access.restricted && !authenticationService.isLoggedIn()) {
      $location.path('/login');
    }
  });
});
